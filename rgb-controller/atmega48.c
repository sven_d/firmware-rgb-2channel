/*
	Alternativ Firmware für RGB-Multiline Controller

 * PB1 (OC1A) PWM Timer1 Kanal 1 Grün1 		Pin 15
 * PB2 (OC1B) PWM Timer1 Kanal 2 Blau1 		Pin 16
 *
 * PB3 (OC2A) PWM Timer2 Kanal 1 Grün2 		Pin 17
 * PD3 (OC2B) PWM Timer2 Kanal 2 Blau2 		Pin 5
 *
 * PD6 (OC0A) PWM Timer0 Kanal 1 Rot1		Pin 12
 * PD5 (OC0B) PWM Timer0 Kanal 1 Rot2 		Pin 11
 *
 * PC0 Programm Kanal 1						Pin 23
 * PC1 Geschwindigkeit Kanal 1				Pin 24
 * PC2 Programm Kanal 2						Pin 25
 * PC3 Geschwindigkeit Kanal 2				Pin	26
 */

#define ARDUINO

#ifdef ARDUINO
#define F_CPU 16000000 //8 Mhz Takt !!! UNBEDINGT VOR DEN INCLUDES braucht delay.h !!!
#else
#define F_CPU 8000000
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
//#include <avr/sleep.h>
#include <stdlib.h>

#include "atmega48.h"

#define FARBVERLAUF 0
#define STOP 		1
#define PINK 		2
#define CYAN 		3
#define GREEN 		4
#define YELLOW 		5
#define BLUE 		6
#define RED 		7
#define RANDOM		8

#define LIMIT_H		255
#define LIMIT_S		56
#define LIMIT_V		255

#define entprellZeit 25

// Hack um Code für AD-WANDLER zu entfernen bzw. nicht zu übersetzen
#define DISABLE_ADC

#define NO_SLEEP

int ticks;
int seeds[8];

struct channelState {
	uint8_t h;
	uint8_t s;
	uint8_t v;
	uint8_t done;
	uint8_t down;
	uint8_t prog;
	uint8_t delay;
	uint8_t num;
};

struct color_rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct delayObj {
	uint8_t delay_ms;
	uint8_t delay_ms_done;
	uint8_t done;
	uint8_t us_counter;
};

struct delayObj delayObjs[3];

uint8_t buttonDownPortC;
uint8_t buttonUpPortC;

// 8 Bit Timer
// PD6 (10) = Kanal A
// PD5 (9) = Kanal B
// für PWM konfigurieren
void setupTimer0(void)
{
	// FAST PWM
	TCCR0A = 0b10100011;
	TCCR0B = 0b00000011;
	//TCCR0A = 0x03;
	//bit_set(TCCR0A,BIT(WGM00));
	//bit_set(TCCR0A,BIT(WGM01));

	//Prescaler F_CPU/32
	//TCCR0B = 0x03;
	//bit_set(TCCR0B,BIT(CS00));
	//bit_set(TCCR0B,BIT(CS01));

	//Counter auf 0 setzen
	TCNT0=0x00;
}

// 16 Bit Timer
// PB1 (13) = Kanal A
// PB2 (14) = Kanal B
// für PWM konfigurieren
void setupTimer1(void)
{
	//FAST PWM
	/*
	bit_set(TCCR1A,BIT(WGM10));
	bit_set(TCCR1B,BIT(WGM12));


	//Prescaler F_CPU/32
	bit_set(TCCR1B,BIT(CS10));
	bit_set(TCCR1B,BIT(CS11));
	*/

	TCCR1A = 0b10100001;
	TCCR1B = 0b00001011;
	//Counter auf 0 setzen
	TCNT1=0x0000; //16 Bit!

	//????????????ICR1 = 1000; // every 1000 uS or 1mS
}

// 8 Bit Timer
// PB3 = Kanal A
// PD3 = Kanal B
// für PWM konfigurieren
void setupTimer2(void)
{
	// FAST PWM
	/*
	bit_set(TCCR2A,BIT(WGM20));
	bit_set(TCCR2A,BIT(WGM21));

	//Prescaler F_CPU/32
	bit_set(TCCR2B,BIT(CS20));
	bit_set(TCCR2B,BIT(CS21));
	*/
	TCCR2A = 0b10100011;
	TCCR2B = 0b00000011;

	TCNT2=0x00;
}

//PD6 auf PWM schalten Timer0
/*void enablePD6(void)
{
	bit_set(TCCR0A,BIT(COM0A1));
	bit_clear(TCCR0A,BIT(COM0A0));
}

//PD5 auf PWM schalten Timer0
void enablePD5(void)
{
	bit_set(TCCR0A,BIT(COM0B1));
	bit_clear(TCCR0A,BIT(COM0B0));
}*/

//PB1 auf PWM schalten Timer 1
/*void enablePB1(void)
{
	bit_set(TCCR1A,BIT(COM1A1));
	bit_clear(TCCR1A,BIT(COM1A0));
}

//PB2 auf PWM schalten Timer 1
void enablePB2(void)
{
	 bit_set(TCCR1A,BIT(COM1B1));
	 bit_clear(TCCR1A,BIT(COM1B0));
}*/

//PB3 auf PWM schalten Timer 2
/*void enablePB3(void)
{
	bit_set(TCCR2A,BIT(COM2A1));
	bit_clear(TCCR2A,BIT(COM2A0));
}*/

//PD3 auf PWM schalten Timer 2
/*void enablePD3(void)
{
	bit_set(TCCR2A,BIT(COM2B1));
	bit_clear(TCCR2A,BIT(COM2B0));
}*/


// Pullup an PC0, PC1, PC2, PC3 aktivieren
// Achtung!!
// Wenn Taster Gedrückt BIT=0!
void setupIO(void)
{
	//PORTC = 0x0F;
	bit_set(PORTC,BIT(PC0));
	bit_set(PORTC,BIT(PC1));
	bit_set(PORTC,BIT(PC2));
	bit_set(PORTC,BIT(PC3));

	bit_set(DDRC,BIT(PC0));
	bit_set(DDRC,BIT(PC1));
	bit_set(DDRC,BIT(PC2));
	bit_set(DDRC,BIT(PC3));
	/*modeCh1CurrState = 0;
	modeCh1LastState = 0;
	modeCh2CurrState = 0;
	modeCh2LastState = 0;

	speedCh1CurrState = 0;
	speedCh1LastState = 0;
	speedCh2CurrState = 0;
	speedCh2LastState = 0;*/
}

// Hack um Code für AD-WANDLER zu entfernen bzw. nicht zu übersetzen
#ifndef DISABLE_ADC
//ADC7 einstellen
void setupADC(void)
{
	//bit_set(ADMUX,BIT(REFS0)); //Interne Referenz
	//bit_set(ADMUX,BIT(REFS1));
	//ADC7 nutzen
	bit_set(ADMUX,BIT(MUX2));
	bit_set(ADMUX,BIT(MUX1));
	bit_set(ADMUX,BIT(MUX0));
}


int analogRead(void)
{
	//uint8_t low, high;

	bit_set(ADCSRA,BIT(ADSC));
	while (bit_get(ADCSRA, ADSC));

	return ADC;
}
#endif
void initDelayObjs()
{
	struct delayObj tmpObj;

	tmpObj.delay_ms = 0;
	tmpObj.delay_ms_done = 0;
	tmpObj.done = 1;

	delayObjs[0] = tmpObj;
	delayObjs[1] = tmpObj;
	delayObjs[2] = tmpObj;
}

/*
 * 0 = Global
 * 1 = Kanal 1
 * 2 = Kanal 2
 */
void scheduleDelay(uint8_t channel, uint8_t ms)
{
	delayObjs[channel].delay_ms = ms;
	delayObjs[channel].delay_ms_done = 0;
	delayObjs[channel].done = 0;
}

void doDelays()
{
	for (uint8_t i = 0; i < 3; i++)
	{
		if (delayObjs[i].delay_ms == delayObjs[i].delay_ms_done)
		{
			delayObjs[i].done = 1;
		}
		else
		{
			if (delayObjs[i].us_counter == 10)
			{
				delayObjs[i].delay_ms_done++;
				delayObjs[i].us_counter = 0;
			}
			else
			{
				delayObjs[i].us_counter++;
			}
		}

	}
	_delay_us(100);
}

/*
H:          der Farbton als Farbwinkel H auf dem Farbkreis (z. B. 0° = Rot, 120° = Grün, 240° = Blau)
S:          die Sättigung S in Prozent (z. B. 0% = keine Farbe, 50% = ungesättigte Farbe, 100% = gesättigte, reine Farbe)
V:          der Grauwert V als Prozentwert angegeben (z. B. 0% = keine Helligkeit, 100% = volle Helligkeit)

Skalierung der HSV Werte:
H:      0-255, 0=rot, 42=gelb, 85=grün, 128=türkis, 171=blau, 214=violett
S:      0-255, 0=weißtöne, 255=volle Farben
V:      0-255, 0=aus, 255=maximale Helligkeit

*/
struct color_rgb hsv_to_rgb (struct channelState *currentColor)//unsigned char h, unsigned char s, unsigned char v, unsigned char channel)
{
	uint8_t r,g,b, i, f;
	unsigned int p, q, t;

	if( currentColor->s == 0 )
	{  r = g = b = currentColor->v;
	}
	else
	{  i=currentColor->h/43;
		f=currentColor->h%43;
		p = (currentColor->v * (255 - currentColor->s))/256;
		q = (currentColor->v * ((10710 - (currentColor->s * f))/42))/256;
		t = (currentColor->v * ((10710 - (currentColor->s * (42 - f)))/42))/256;

		switch( i )
		{  case 0:
			r = currentColor->v; g = t; b = p; break;
			case 1:
			r = q; g = currentColor->v; b = p; break;
			case 2:
			r = p; g = currentColor->v; b = t; break;
			case 3:
			r = p; g = q; b = currentColor->v; break;
			case 4:
			r = t; g = p; b = currentColor->v; break;
			case 5:
			r = currentColor->v; g = p; b = q; break;
		}
	}

	struct color_rgb color;
	color.r = r;
	color.g = g;
	color.b = b;

	return color;
}

void checkButton(uint8_t pin)
{
	if (bit_get(PINC,BIT(pin))== 0) // gedrückt
	{
		bit_set(buttonDownPortC,BIT(pin));
	}
	else //nicht gedrückt
	{
		if ((buttonDownPortC & (0x01 << pin)) != 0)
		{
			bit_set(buttonUpPortC,BIT(pin));
		}
	}
}
void checkButtons()
{
	checkButton(PC0);
	checkButton(PC1);
	checkButton(PC2);
	checkButton(PC3);
	scheduleDelay(0,entprellZeit);
}

unsigned char wasButtonPressed(uint8_t pin)
{
	uint8_t state;

	state = buttonDownPortC & buttonUpPortC;

	if ((state & (0x01 << pin)) != 0)
	{
		bit_clear(buttonDownPortC,BIT(pin));
		bit_clear(buttonUpPortC,BIT(pin));
		return 1;
	}
	return 0;
}

void writeValues(struct channelState *state)
{

	struct color_rgb rgb = hsv_to_rgb(state);
	if (state->num == 1)
	{
		writeRed1(rgb.r);
		writeGreen1(rgb.g);
		writeBlue1(rgb.b);
	}
	else if (state->num == 2)
	{
		writeRed2(rgb.r);
		writeGreen2(rgb.g);
		writeBlue2(rgb.b);
	}
}

// von aktuellem Farbwert zum Farbwert TargetH, S ,V wechseln

// gibt den neuen HSV-Farbwert zurück
void fadeToColor(struct channelState *currentColor, struct channelState targetColor)//unsigned char channel)
{


	if (currentColor->s < targetColor.s)
	{
		currentColor->done = 0;
		currentColor->s++;
	}
	else if (currentColor->s > targetColor.s)
	{
		currentColor->done = 0;
		currentColor->s--;
	}
	else if (currentColor->s == targetColor.s)
	{
		if (currentColor->v < targetColor.v)
		{
			currentColor->done = 0;
			currentColor->v++;
		}
		else if (currentColor->v > targetColor.v)
		{
			currentColor->done = 0;
			currentColor->v--;
		}
		else if (currentColor->v == targetColor.v)
		{
			if (currentColor->h < targetColor.h)
			{
				currentColor->done = 0;
				currentColor->h++;
			}
			else if (currentColor->h > targetColor.h)
			{
				currentColor->done = 0;
				currentColor->h--;
			}
			else if (currentColor->h == targetColor.h)
			{
				currentColor->done = 1;
			}
		}
	}
}

void farbVerlauf(struct channelState *currentColor)
{
	currentColor->s = 255;
	currentColor->v = 255;

	if (currentColor->h == 255)
	{
		currentColor->down = 1;
	}
	else if (currentColor->h == 0)
	{
		currentColor->down = 0;
	}

	if (currentColor->down == 1)
	{
		currentColor->h--;
	}
	else
	{
		currentColor->h++;
	}

}

unsigned char getSeed(uint8_t seed)
{
	srand(seeds[seed]);
	return seeds[(rand()%7)];
}

void randomColor(struct channelState *currentColor)
{
	uint8_t nextseed;
	struct channelState targetColor;

	if (currentColor->done == 1)
	{
		srand(ticks);
		nextseed = (rand()%7);
		seeds[nextseed]=ticks;

		nextseed = getSeed(seeds[nextseed]);
		srand(seeds[nextseed]);
		targetColor.h = (rand() % 256);

		nextseed = getSeed(seeds[nextseed]);
		srand(seeds[nextseed]);
		targetColor.s = (rand() % 200) + LIMIT_S;

		nextseed = getSeed(seeds[nextseed]);
		srand(seeds[nextseed]);
		targetColor.v = (rand() % 256) + LIMIT_V;
	}

	fadeToColor(currentColor, targetColor);
}

void pink(struct channelState *currentColor)
{

	struct channelState color;
	color.h = 220;
	color.s = 255;
	color.v = 255;

	fadeToColor(currentColor, color);
}

void cyan(struct channelState *currentColor)
{
	struct channelState color;
	color.h = 128;
	color.s = 255;
	color.v = 255;

	fadeToColor(currentColor, color);
}

void green(struct channelState *currentColor)
{
	struct channelState color;
	color.h = 85;
	color.s = 255;
	color.v = 255;

	fadeToColor(currentColor, color);
}

void yellow(struct channelState *currentColor)
{
	struct channelState color;
	color.h = 42;
	color.s = 255;
	color.v = 255;

	fadeToColor(currentColor, color);
}

void blue(struct channelState *currentColor)
{
	struct channelState color;
	color.h = 171;
	color.s = 255;
	color.v = 255;

	fadeToColor(currentColor, color);
}

void red(struct channelState *currentColor)
{
	struct channelState color;
	color.h = 0;
	color.s = 255;
	color.v = 255;

	fadeToColor(currentColor, color);
}



// TODO: Sleep Mode hinzufügen Interrupt und ISR nicht vergessen
#ifndef NO_SLEEP
void off(void)
{
        // Ausgänge nullen
		unsigned char tmp = 0x00;
        writeRed1(tmp);
        writeGreen1(tmp);
        writeBlue1(tmp);
		cli();


		//Vorbereitungen für "power down sleep modus"
        //scheint nicht zu laufen..... !?!?!
		//bit_set(PCICR,PCIE1); //PCIE1 für PINS PCINT14..8 aktivieren
        //bit_set(PCMSK1,0); //PCINT8 aktivieren (alternativ Funktion von PC0 (mode-taster))


		PCICR  |=  (1 << PCIE1);
		PCMSK1 |=  (1 << PCINT8);


        sei(); //Interrupts aktivieren
        set_sleep_mode(SLEEP_MODE_PWR_DOWN); // Power_DOWN_SLEEP

        sleep_mode(); //schlafen gehen

        // wir sollten nach dem Interrupt hier wieder aufwachen
        cli(); //Interrupts löschen
        bit_clear(PCICR,BIT(PCIE1)); //bits zurück setzen
        bit_clear(PCMSK1,BIT(PCINT8));

        state1 = FARBVERLAUF;

}


ISR(PCINT1_vect)
{
}
#endif

void prepare(struct channelState *channel)
{
	switch (channel->prog)
		{
			case FARBVERLAUF:
				farbVerlauf(channel);
				seeds[4]=ticks;
				break;

			case STOP:
				seeds[0]=ticks;
				break;

			case PINK:
				pink(channel);
				seeds[5]=ticks;
				break;

			case CYAN:
				cyan(channel);
				seeds[3]=ticks;
				break;

			case GREEN:
				green(channel);
				seeds[7]=ticks;
				break;

			case YELLOW:
				yellow(channel);
				seeds[1]=ticks;
				break;

			case BLUE:
				blue(channel);
				seeds[2]=ticks;
				break;

			case RED:
				red(channel);
				seeds[6]=ticks;
				break;

			case RANDOM:
				randomColor(channel);
				break;

		}
		if (channel->prog == FARBVERLAUF || channel->prog == RANDOM)
		{
			/*delay_10ms(channel->delay);
			writeValues(channel);
			*/
			scheduleDelay(channel->num, channel->delay*25);

		}
		else
		{
			scheduleDelay(channel->num,5);
			/*delay_10ms(1);
			writeValues(channel);
			*/

		}
		//return channel;
}

int main(void)
{
	ticks = 0;
	setupTimer0();
	setupTimer1();
	setupTimer2();
	setupIO();
	//setupADC();
	// Pins PB1, PB2, PB3, PD3, PD5, PD6 auf ausgang schalten

	// Timer 1
	/*
	bit_set(DDRB,BIT(PB1));
	bit_set(DDRB,BIT(PB2));

	//Timer 0
	bit_set(DDRD,BIT(PD6));
	bit_set(DDRD,BIT(PD5));

	//Timer2
	bit_set(DDRD,BIT(PD3));
	bit_set(DDRB,BIT(PB3));
	*/

	DDRB = 0b00001110;
	DDRD = 0b01101000;

	// PWM aktivieren
	//enablePD6();
	//enablePD5();

	//enablePB1();
	//enablePB2();

	//enablePB3();
	//enablePD3();


	//H1=0;
	//S1=255;
	//V1=255;
	struct channelState stateCh1;
	struct channelState stateCh2;
	struct channelState *p_stateCh1;
	struct channelState *p_stateCh2;
	stateCh1.h = 0;
	stateCh1.s = 255;
	stateCh1.v = 255;
	stateCh1.done = 0;
	stateCh1.down = 0;
	stateCh1.delay = 1;
	stateCh1.prog = 0;
	stateCh1.num = 1;


	stateCh2.h = 0;
	stateCh2.s = 255;
	stateCh2.v = 255;
	stateCh2.done = 0;
	stateCh2.down = 0;
	stateCh2.delay = 1;
	stateCh2.prog = 0;
	stateCh2.num = 2;

	//Down1 = 0; //farbverlauf geht aufwärts
	//state1 = 0; //state1 = 0 = farbverlauf
	//state2 = 0;
	//delaymultCh1 = 1;
	//delaymultCh2 = 1;
	//###### main Loop ######
	// pollen für Tasterdruck
	// implementierung der Statemachine

	p_stateCh1 = &stateCh1;
	p_stateCh2 = &stateCh2;

	initDelayObjs();

	prepare(p_stateCh1);
	prepare(p_stateCh2);
	checkButtons();
	while(1)
	{
		ticks++;
		/*if (isButtonDelayCH1Pressed())
		{
			if (p_stateCh1->delay <= 8)
				p_stateCh1->delay ++;
			else
				p_stateCh1->delay = 1;
		}
		if (isButtonDelayCH2Pressed())
		{
			if (p_stateCh2->delay <= 8)
				p_stateCh2->delay ++;
			else
				p_stateCh2->delay = 1;
		}

		if (isButtonModeCH1Pressed())
		{
			if (p_stateCh1->prog == 8)
				p_stateCh1->prog = 0;
			else
				p_stateCh1->prog++;
		}

		if (isButtonModeCH2Pressed())
		{
			if (p_stateCh2->prog == 8)
				p_stateCh2->prog = 0;
			else
				p_stateCh2->prog++;
		}*/

		if (wasButtonPressed(PC0))
		{
			if (p_stateCh1->prog == 8)
				p_stateCh1->prog = 0;
			else
				p_stateCh1->prog++;
		}

		if (wasButtonPressed(PC1))
		{
			if (p_stateCh1->delay <= 8)
				p_stateCh1->delay ++;
			else
				p_stateCh1->delay = 1;
		}

		if (wasButtonPressed(PC2))
		{
			if (p_stateCh2->prog == 8)
				p_stateCh2->prog = 0;
			else
				p_stateCh2->prog++;
		}


		if (wasButtonPressed(PC3))
		{
			if (p_stateCh2->delay <= 8)
				p_stateCh2->delay ++;
			else
				p_stateCh2->delay = 1;
		}




		if (delayObjs[0].done == 1)
		{
			checkButtons();
		}

		if (delayObjs[1].done == 1)
		{
			writeValues(p_stateCh1);
			prepare(p_stateCh1);
		}

		if (delayObjs[2].done == 1)
		{
			writeValues(p_stateCh2);
			prepare(p_stateCh2);
		}
		doDelays();

	}
}
