/*
 * atmega48.h
 *
 *  Created on: 14.07.2013
 *      Author: sven
 */

#ifndef ATMEGA48_H_
#define ATMEGA48_H_

#define bit_set(p,m) ((p) |= (m))
#define bit_clear(p,m) ((p) &= ~(m))
#define bit_get(p,m) ((p) & (m))
#define BIT(x) (0x01 << (x))

#define pwmSetPD6(val)  OCR0A=val
#define pwmSetPD5(val)  OCR0B=val
#define pwmSetPB1(val)  OCR1A=val
#define pwmSetPB2(val)  OCR1B=val
#define pwmSetPB3(val)	OCR2A=val
#define pwmSetPD3(val)	OCR2B=val

#define writeRed1(val)		pwmSetPD6(val)
#define writeGreen1(val) 	pwmSetPB1(val)
#define writeBlue1(val)		pwmSetPB2(val)
#define writeRed2(val)		pwmSetPD5(val)
#define writeGreen2(val)	pwmSetPB3(val)
#define writeBlue2(val)		pwmSetPD3(val)

#endif /* ATMEGA48_H_ */
